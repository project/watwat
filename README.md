CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Watwat module was created to import the articles from WAT WAT (http://www.watwat.be) to other Drupal sites.


REQUIREMENTS
------------

This module requires the following modules:

  * Migrate tools (https://www.drupal.org/project/migrate_tools)
  * Chosen (https://www.drupal.org/project/chosen) -> make sure the library gets installed correctly.

The core module Migrate should be enabled.

If you want to migrate the articles, the following modules are required:
  * Paragraphs (https://www.drupal.org/project/paragraphs)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Watwat exists of 3 submodules:
- watwat_browser
- watwat_paragraphs
- watwat_article

Enable the watwat_browser submodule for viewing the articles and selecting articles for migration.
Enable the watwat_article submodule for migrating articles to your website.
Watwat_paragraphs is enabled when the submodule watwat_article is enabled.

You can use this module for the browser only and write your own migrations, for instance if you only want to use the teasers.

How this module works:
Select articles in the api browser.
You can access this through /admin/content/watwat.
The articles marked for migration will be migrated by triggering the WatwatMigrationScript in the scripts directory.
Nodes of the type 'watwat_article' will be created by the migration, with the same content as the corresponding article on watwat.be.


**If you want to trigger the migrations manually, you have to do it in the following order:**
First, the paragraphs:
`watwat_paragraph_text_migration
watwat_paragraph_video_migration
watwat_paragraph_quote_migration
watwat_paragraph_html_embed_migration
watwat_paragraph_faq_item_migration
watwat_paragraph_faq_migration
watwat_paragraph_image_file_migration
watwat_paragraph_image_media_migration
watwat_paragraph_image_migration
watwat_paragraph_cta_file_migration
watwat_paragraph_cta_media_migration
watwat_paragraph_call_to_action_migration
watwat_paragraph_attachment_file_migration
watwat_paragraph_attachment_item_migration
watwat_paragraph_attachment_migration
`

Next the article and its image:
`watwat_article_image_file_migration
watwat_article_image_media_migration
watwat_article_migration`

You can also trigger the paragraphs migrations by their groups in the following order:
`watwat_paragraphs_files
watwat_praragraphs_prep
watwat_paragraphs`


MAINTAINERS
-----------

Current maintainers:
 * 3Sign - http://3sign.com

This project has been sponsored by:
 * WAT WAT - http://www.watwat.be

