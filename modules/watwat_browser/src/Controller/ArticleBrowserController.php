<?php

namespace Drupal\watwat_browser\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\watwat\WatWatApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ArticleBrowserController.
 *
 * @package Drupal\watwat_browser\Controller
 */
class ArticleBrowserController extends ControllerBase {

  /**
   * Drupal\watwat\WatWatApiService.
   *
   * @var \Drupal\watwat\WatWatApiService
   */
  protected $watwatApiService;

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * ArticleBrowserController constructor.
   *
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Api service for collecting themes and articles.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The Form builder.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager interface.
   */
  public function __construct(WatWatApiService $watwat_api_service,
                              FormBuilderInterface $form_builder,
                              RequestStack $request,
                              EntityTypeManagerInterface $entity_type_manager,
                              PagerManagerInterface $pager_manager) {
    $this->watwatApiService = $watwat_api_service;
    $this->formBuilder = $form_builder;
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
    $this->pagerManager = $pager_manager;
  }

  /**
   * Creates an instance of the controller.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The ContainerInterface.
   *
   * @return ArticleBrowserController
   *   The article browser controller.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('watwat.apiService'),
      $container->get('form_builder'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('pager.manager')
    );
  }

  /**
   * Returns a renderable array for every article.
   */
  public function content() {
    // Build the Api Browser Form.
    $build['form'] = $this->formBuilder->getForm('\Drupal\watwat_browser\Form\ApiBrowserForm');

    // Get all the parameters from the url.
    $params = $this->request->getCurrentRequest()->query->all();
    $params['amount'] = 12;

    // Get the articles based on the query parameters in the url.
    $api_articles = $this->watwatApiService->getArticles($params);

    $watwat_base_url = $GLOBALS['WATWAT_BASE_URL'];

    if (!empty($api_articles->results)) {
      // Get the total amount of articles and initialize the pager.
      $total = $api_articles->total;
      $page = $this->pagerManager->createPager($total, $params['amount']);
      $params['offset'] = $params['amount'] * $page->getCurrentPage();

      // Get the articles based on the updated query parameters and the pager.
      $api_articles = $this->watwatApiService->getArticles($params);
      $articles = $api_articles->results;

      // Create articles wrapper.
      $build['articles'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['watwat__articles'],
        ],
        '#attached' => [
          'library' => ['watwat_browser/watwat_browser'],
        ],
      ];

      foreach ($articles as $article) {
        $content = [];
        $content['article'] = $article;

        // Add the button for adding the articles.
        $content['addtomigration_form'] = $this->formBuilder->getForm('\Drupal\watwat_browser\Form\ArticleMigrationForm', [$article->id]);

        // Get selected status of the article.
        $content['watwat_article_status'] = $content['addtomigration_form']['watwat_article_status']['#default_value'];

        // Add the url to the WAT WAT website.
        $content['watwat_url'] = $watwat_base_url . 'node/' . $article->id;

        // Add the primary theme.
        $primary_theme_id = $article->primaryTheme;
        $primary_theme = $this->getWatWatThemeTerm($primary_theme_id);
        if (!empty($primary_theme)) {
          $content['primary_theme'] = $primary_theme->getName();
        }

        // Add the secondary themes.
        if (isset($article->secondaryThemes)) {
          $secondary_theme_ids = $article->secondaryThemes;
          foreach ($secondary_theme_ids as $watwat_theme_id) {
            $watwat_theme = $this->getWatWatThemeTerm($watwat_theme_id);
            if (!empty($watwat_theme)) {
              $content['secondary_theme'][] = $watwat_theme->getName();
            }
          }
        }

        // Build the article with the template.
        $build['articles'][] = [
          '#theme' => 'browser_articles',
          '#content' => $content,
          '#cache' => [
            'contexts' => [
              'url.path',
            ],
          ],
        ];
      }
      // Add the pager under the articles.
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    else {
      $build['articles'] = [
        '#markup' => $this->t('No articles were found.'),
      ];
    }

    return $build;
  }

  /**
   * Function to load the Watwat themes term by the field_watwat_theme_id.
   *
   * @param int $term_id
   *   The taxonomy term id.
   *
   * @return object
   *   The taxonomy term.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getWatWatThemeTerm($term_id) {
    $watwat_theme = [];

    // Load the term by the value of the Watwat theme id.
    $taxonomy_term_manager = $this->entityTypeManager->getStorage('taxonomy_term');
    $themes = $taxonomy_term_manager->loadByProperties([
      'field_watwat_theme_id' => $term_id,
    ]);

    if (!empty($themes)) {
      $watwat_theme = reset($themes);
    }
    return $watwat_theme;
  }

}
