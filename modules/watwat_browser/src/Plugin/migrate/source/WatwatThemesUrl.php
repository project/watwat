<?php

namespace Drupal\watwat_browser\Plugin\migrate\source;

use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;
use Drupal\watwat\WatWatApiService;
use Drupal\watwat\WatwatMissingSourceInterface;
use Drupal\watwat\WatwatMissingSourceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Source plugin for retrieving Wat Wat themes through the URLs.
 *
 * @MigrateSource(
 *   id = "watwat_themes_url"
 * )
 */
class WatwatThemesUrl extends Url implements ContainerFactoryPluginInterface, WatwatMissingSourceInterface {

  use WatwatMissingSourceTrait;

  /**
   * Drupal\watwat\WatWatApiService.
   *
   * @var \Drupal\watwat\WatWatApiService
   */
  protected $watwatApiService;

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * WatwatThemesUrl constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory) {

    // Get the total item of themes from the api.
    $total_themes = 0;
    $this->watwatApiService = $watwat_api_service;
    $this->loggerFactory = $logger_factory;
    $watwat_themes = $this->watwatApiService->getResults('themes', []);

    if (!empty($watwat_themes)) {
      $total_themes = $watwat_themes->total;
    }
    else {
      // Log an error.
      $message = 'No themes were found for the Wat Wat themes migration.';
      $this->loggerFactory->get('watwat')->error($message);
    }

    // Add extra urls depending on the total amount of themes.
    $i = 0;
    if (!empty($configuration['urls']) && is_string($configuration['urls'])) {
      $base_url = $configuration['urls'];
      $configuration['urls'] = [];
      while ($total_themes > $i) {
        if ($total_themes > $i) {
          $configuration['urls'][] = $base_url . '&offset=' . $i;
        }
        $i += 100;
      }
    }
    else {
      // Log an error.
      $message = 'No url was configured for the Wat Wat themes migration.';
      $this->loggerFactory->get('watwat')->error($message);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * Creates an instance of the class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container interface.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface|null $migration
   *   The migration interface.
   *
   * @return WatwatThemesUrl|static
   *   The Watwat themes url.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('watwat.apiService'),
      $container->get('logger.factory')
    );
  }

}
