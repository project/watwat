<?php

namespace Drupal\watwat_browser\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;

/**
 * Class ApiBrowserForm.
 *
 * @package Drupal\watwat_browser\Form
 */
class ApiBrowserForm extends FormBase {

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\migrate\Plugin\MigrationPluginManager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationManager;

  /**
   * ApiBrowserForm constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The Request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager interface.
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $migration_manager
   *   The Migration plugin manager.
   */
  public function __construct(RequestStack $request,
                              EntityTypeManagerInterface $entity_type_manager,
                              MigrationPluginManager $migration_manager) {
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
    $this->migrationManager = $migration_manager;
  }

  /**
   * Creates an instance of the form.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The ContainerInterface.
   *
   * @return \Drupal\watwat_browser\Form\ApiBrowserForm
   *   The Api Browser form.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'watwat_api_browser';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the query parameters to use as default values.
    $default = $this->request->getCurrentRequest()->query->all();

    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form--inline', 'clearfix']],
    ];

    $form['filters']['query'] = [
      '#type' => 'textfield',
      '#title' => 'Search word',
      '#default_value' => isset($default['query']) ? $default['query'] : '',
    ];

    $form['filters']['minAge'] = [
      '#type' => 'number',
      '#title' => 'Minimum age',
      '#default_value' => isset($default['minAge']) ? $default['minAge'] : NULL,
      '#min' => 11,
      '#max' => 24,
    ];

    $form['filters']['maxAge'] = [
      '#type' => 'number',
      '#title' => 'Maximum age',
      '#default_value' => isset($default['maxAge']) ? $default['maxAge'] : NULL,
      '#min' => 11,
      '#max' => 24,
    ];

    // Load the Wat Wat themes from the vocabulary.
    $themes_voc = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('watwat_themes');
    $watwat_themes = [];

    if (empty($themes_voc)) {
      // Run the themes migration.
      $migration = $this->migrationManager->createInstance('watwat_themes_migration');
      if (!empty($migration)) {
        $executable = new MigrateExecutable($migration, new MigrateMessage());
        $executable->import();
        $themes_voc = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('watwat_themes');
      }
    }

    if (!empty($themes_voc)) {
      foreach ($themes_voc as $theme) {
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($theme->tid);

        // Get the names of each theme.
        if ($term->hasField('field_watwat_theme_id')) {
          $watwat_themes[$term->field_watwat_theme_id->value] = $theme->name;
        }
      }

      // Make an array of the themes that were selected.
      if (!empty($default['themes'])) {
        $default_themes = explode('+', $default['themes']);
      }

      $form['filters']['themes'] = [
        '#type' => 'select',
        '#title' => 'Themes',
        '#options' => $watwat_themes,
        '#multiple' => TRUE,
        '#default_value' => isset($default_themes) ? $default_themes : NULL,
        '#chosen' => TRUE,
      ];
    }

    $form['actions']['#type'] = 'actions';

    // Render the submit button.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search articles'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Add query parameters to the url.
    $params = [];
    if (!empty($form_state->getValues())) {
      $params['query'] = $form_state->getValue('query');
      $params['minAge'] = $form_state->getValue('minAge');
      $params['maxAge'] = $form_state->getValue('maxAge');
      $themes = $form_state->getValue('themes');
      $params['themes'] = implode('+', $themes);
      $params['offset'] = $form_state->getValue('offset');
      $params['amount'] = $form_state->getValue('amount');
    }

    // Redirect to the controller with the parameters.
    $form_state->setRedirect('watwat.api_browser', $params);
  }

}
