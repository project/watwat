<?php

namespace Drupal\watwat_browser\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ArticleMigrationForm.
 *
 * @package Drupal\watwat_browser\Form
 */
class ArticleMigrationForm extends FormBase {

  /**
   * Keep track of how many times the form is placed on a page.
   *
   * @var int
   */
  protected static $instanceId;

  /**
   * Drupal\Component\Datetime\TimeInterface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * ArticleMigrationForm constructor.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time interface.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger channel factory.
   */
  public function __construct(TimeInterface $time,
                              Connection $connection,
                              EntityTypeManagerInterface $entityTypeManager,
                              LoggerChannelFactory $loggerFactory) {
    $this->time = $time;
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Creates an instance of the form.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The ContainerInterface.
   *
   * @return \Drupal\watwat_browser\Form\ArticleMigrationForm
   *   The article migration form.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    if (empty(self::$instanceId)) {
      self::$instanceId = 1;
    }
    else {
      self::$instanceId++;
    }
    return 'article_migration_form ' . self::$instanceId;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $article_id = []) {
    $status_default_value = '';

    // Check if the id is already in the addtomigration table.
    $addtomigration_status = $this->getMigrationStatus($article_id[0]);

    if (!empty($addtomigration_status)) {
      $migration_text = 'Remove';

      // Set the status value for the template to selected.
      $status_default_value = 'watwat__article--selected';

      // Add the link to the article in the current website.
      $watwat_source = reset($addtomigration_status);
      $source_id = intval($watwat_source->watwat_source_id);
      $node_id = $this->getMigratedNodeId($source_id);

      // If there is a node id, set the default value to migrated.
      if (!empty($node_id)) {
        $status_default_value = 'watwat__article--migrated';
      }
    }
    else {
      $migration_text = 'Add to migration';
    }

    $form['watwat_article_status'] = [
      '#type' => 'hidden',
      '#default_value' => $status_default_value,
    ];

    $form['article_id'] = [
      '#type' => 'hidden',
      '#default_value' => $article_id[0],
    ];

    $form['actions']['#type'] = 'actions';

    // Render the button.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $migration_text,
    ];

    if (!empty($node_id)) {
      $form['node_url'] = [
        '#type' => 'link',
        '#title' => $this->t('View this article on this site'),
        '#url' => Url::fromRoute('entity.node.canonical', ['node' => $node_id]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $timestamp = $this->time->getCurrentTime();
    $article_id = $form_state->getValue('article_id');

    $addtomigration_status = $this->getMigrationStatus($article_id);

    if ($addtomigration_status != FALSE) {
      // Remove the id from the watwat addtomigration table.
      $this->connection->delete('watwat_addtomigration')
        ->condition('watwat_source_id', $article_id)
        ->execute();

      // Log an message.
      $message = 'The WAT WAT article with id: ' . $article_id . ' was removed from the migration.';
      $this->loggerFactory->get('watwat_browser')->notice($message);

      // Unpublish the article node.
      $this->setPublishedStatus($article_id, FALSE);
    }
    else {
      // Add id to table.
      $this->connection->insert('watwat_addtomigration')
        ->fields(['watwat_source_id', 'watwat_timestamp'], [$article_id, $timestamp])
        ->execute();

      // Log an message.
      $message = 'The WAT WAT article with id: ' . $article_id . ' was added to the migration.';
      $this->loggerFactory->get('watwat_browser')->notice($message);

      // If the node already exists, publish the node again.
      if (!empty($this->getMigratedNodeId($article_id))) {
        $this->setPublishedStatus($article_id, TRUE);
      }
    }
  }

  /**
   * Check if the id is in the watwat_addtomigration database.
   *
   * @param int $article_id
   *   The article id on WAT WAT.
   *
   * @return mixed
   *   Returns the result of the query.
   */
  public function getMigrationStatus($article_id) {
    $article_query = $this->connection->select('watwat_addtomigration', 'n')
      ->fields('n', ['watwat_source_id', 'watwat_timestamp'])
      ->condition('watwat_source_id', $article_id)
      ->execute()
      ->fetchAll();

    return $article_query;
  }

  /**
   * Get the current node id for the migrated article.
   *
   * @param int $article_id
   *   The article id on WAT WAT.
   *
   * @return mixed
   *   Returns the node id on this site, or returns empty.
   */
  public function getMigratedNodeId($article_id) {
    if (Database::getConnection()->schema()->tableExists('migrate_map_watwat_article_migration')) {
      $node_query = $this->connection->select('migrate_map_watwat_article_migration', 'm')
        ->fields('m', ['destid1'])
        ->condition('sourceid1', $article_id)
        ->execute()
        ->fetchAll();

      if (!empty($node_query)) {
        $first_node = reset($node_query);
        $node_id = $first_node->destid1;

        return $node_id;
      }
    }
  }

  /**
   * Set the published status for the migrated article.
   *
   * @param int $article_id
   *   The article id on WAT WAT.
   * @param bool $status
   *   The required status for the article.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setPublishedStatus($article_id, $status) {
    $node_manager = $this->entityTypeManager->getStorage('node');
    $entity_id = $this->getMigratedNodeId($article_id);

    if (!empty($entity_id)) {
      $article = $node_manager->load($entity_id);
      if (!empty($article)) {
        $article->setPublished($status);
        $article->save();

        // Log an message.
        $message = 'The published status for the WAT WAT article with node id: ' . $entity_id . ' was set to ' . $status . '.';
        $this->loggerFactory->get('watwat_browser')->notice($message);
      }
    }
  }

}
