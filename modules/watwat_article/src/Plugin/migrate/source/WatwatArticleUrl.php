<?php

namespace Drupal\watwat_article\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\watwat\Plugin\migrate\source\WatwatUrlBase;
use Drupal\watwat\WatwatMissingSourceInterface;
use Drupal\watwat\WatwatMissingSourceTrait;

/**
 * Source plugin for retrieving data via URLs.
 *
 * @MigrateSource(
 *   id = "watwat_article_url"
 * )
 */
class WatwatArticleUrl extends WatwatUrlBase implements WatwatMissingSourceInterface {

  use WatwatMissingSourceTrait;

  /**
   * Adds additional data to the row.
   *
   * @param \Drupal\Migrate\Row $row
   *   The row object.
   *
   * @return bool
   *   FALSE if this row needs to be skipped.
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    $target_ids = [];

    $paragraphs = $row->getSourceProperty('paragraphs');
    if (!empty($paragraphs)) {
      // Get the target and target revision ids for every paragraph.
      foreach ($paragraphs as $paragraph) {
        $database_table = 'migrate_map_watwat_paragraph_' . $paragraph['type'] . '_migration';

        if ($this->connection->schema()->tableExists($database_table) === TRUE) {
          $results = $this->connection->select($database_table, 'yt')
            ->fields('yt', ['destid1', 'destid2'])
            ->condition('yt.sourceid1', $row->getSourceProperty('nid'), '=')
            ->condition('yt.sourceid2', $paragraph['id'], '=')
            ->execute()
            ->fetchAll();
          if (!empty($results)) {
            foreach ($results as $result) {
              $target_ids[] = [
                'target_id' => $result->destid1,
                'target_revision_id' => $result->destid2,
              ];
            }
          }
        }
        else {
          // Log an error.
          $message = 'The database table ' . $database_table . ' was not found.';
          $this->loggerFactory->get('watwat')->error($message);
        }
      }
    }
    // Set the new source property for the row.
    $row->setSourceProperty('prepare_paragraph', $target_ids);
    return parent::prepareRow($row);
  }

}
