<?php

namespace Drupal\watwat_paragraphs\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\watwat\WatWatApiService;
use Drupal\watwat\WatwatMissingSourceInterface;
use Drupal\watwat\WatwatMissingSourceTrait;

/**
 * Source plugin for retrieving the file for image paragraphs.
 *
 * @MigrateSource(
 *   id = "watwat_paragraph_image_file"
 * )
 */
class WatwatParagraphImageFile extends WatwatParagraphSourceBase implements ContainerFactoryPluginInterface, WatwatMissingSourceInterface {

  use WatwatMissingSourceTrait;

  /**
   * WatwatParagraphImageFile constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory,
                              Connection $connection,
                              EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $watwat_api_service, $logger_factory, $connection, $entityTypeManager);

    $paragraphs = [];
    if (!empty($this->articles)) {
      foreach ($this->articles['results'] as $article) {
        $article_content = $article->content;
        foreach ($article_content as $paragraph) {
          if ($paragraph->type == 'image') {
            $paragraphs[] = [
              'Nid' => $article->id,
              'Original id' => $paragraph->id,
              'Weight' => $paragraph->weight,
              'Description' => $paragraph->content->image->title,
              'Image file' => $paragraph->content->image->file,
              'Alt' => $paragraph->content->image->alt,
              'Copyright' => $paragraph->content->image->copyright,
            ];
          }
        }
      }
    }
    $this->dataRows = $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'Nid' => 'Node id',
      'Original id' => 'Original id',
      'Weight' => 'Weight',
      'Description' => 'Description',
      'Image file' => 'Image file',
      'Alt' => 'Alt',
      'Copyright' => 'Copyright',
    ];
  }

}
