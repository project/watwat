<?php

namespace Drupal\watwat_paragraphs\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\watwat\WatWatApiService;
use Drupal\watwat\WatwatMissingSourceInterface;
use Drupal\watwat\WatwatMissingSourceTrait;

/**
 * Source plugin for retrieving the call to action paragraphs.
 *
 * @MigrateSource(
 *   id = "watwat_paragraph_cta"
 * )
 */
class WatwatParagraphCallToAction extends WatwatParagraphSourceBase implements ContainerFactoryPluginInterface, WatwatMissingSourceInterface {

  use WatwatMissingSourceTrait;

  /**
   * WatwatParagraphCallToAction constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory,
                              Connection $connection,
                              EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $watwat_api_service, $logger_factory, $connection, $entityTypeManager);

    $paragraphs = [];
    $watwat_base_url = $GLOBALS['WATWAT_BASE_URL'];
    if (!empty($this->articles)) {
      foreach ($this->articles['results'] as $article) {
        $article_content = $article->content;
        foreach ($article_content as $paragraph) {
          if ($paragraph->type == 'call_to_action') {
            // Check for and update internal urls.
            if ($paragraph->content->link->external === FALSE) {
              $cta_url = $watwat_base_url . $paragraph->content->link->url;
            }
            else {
              $cta_url = $paragraph->content->link->url;
            }

            $fields = [
              'Nid' => $article->id,
              'Original id' => $paragraph->id,
              'Weight' => $paragraph->weight,
              'Title' => $paragraph->content->title,
              'Description' => $paragraph->content->description,
              'Url' => $cta_url,
              'Link text' => $paragraph->content->link->text,
            ];

            // Add the image fields if an image field is present.
            if (!empty($paragraph->content->image)) {
              $fields['Image title'] = $paragraph->content->image->title;
              $fields['Image'] = $paragraph->content->image->file;
            }
            $paragraphs[] = $fields;
          }
        }
      }
    }
    $this->dataRows = $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'Nid' => 'Node id',
      'Original id' => 'Original id',
      'Weight' => 'Weight',
      'Title' => 'Title',
      'Description' => 'Description',
      'Image title' => 'Image title',
      'Image' => 'Image',
      'Url' => 'Url',
      'Link text' => 'Link text',
    ];
  }

  /**
   * Adds additional data to the row.
   *
   * @param \Drupal\Migrate\Row $row
   *   The row object.
   *
   * @return bool
   *   FALSE if this row needs to be skipped.
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    $target_ids = [];
    $database_table = 'migrate_map_watwat_paragraph_cta_media_migration';

    // Get the target ids for the image.
    if ($this->connection->schema()->tableExists($database_table) === TRUE) {
      $results = $this->connection->select($database_table, 'yt')
        ->fields('yt', ['destid1'])
        ->condition('yt.sourceid1', $row->getSourceProperty('Nid'), '=')
        ->condition('yt.sourceid2', $row->getSourceProperty('Original id'), '=')
        ->execute()
        ->fetchAll();
      if (!empty($results)) {
        foreach ($results as $result) {
          $target_ids[] = [
            'target_id' => $result->destid1,
          ];
        }
      }
    }
    else {
      // Log an error.
      $message = 'The database table ' . $database_table . ' was not found.';
      $this->loggerFactory->get('watwat')->error($message);
    }

    // Add the target ids as a source property for the row.
    $row->setSourceProperty('prepare_image', $target_ids);

    // Update the internal links to external links for the description.
    $source_description = $row->getSourceProperty('Description');
    if ($source_description) {
      $transformed_description = $this->transformString($source_description);
      $row->setSourceProperty('Description', $transformed_description);
    }

    return parent::prepareRow($row);
  }

}
