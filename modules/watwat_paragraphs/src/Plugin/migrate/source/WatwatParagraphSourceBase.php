<?php

namespace Drupal\watwat_paragraphs\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\SourcePluginExtension;
use Drupal\watwat\WatWatApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for the source plugins for the paragraphs.
 */
class WatwatParagraphSourceBase extends SourcePluginExtension implements ContainerFactoryPluginInterface {

  /**
   * Data obtained from the source plugin configuration.
   *
   * @var array[]
   *   Array of data rows, each one an array of values keyed by field names.
   */
  protected $dataRows = [];

  /**
   * Data obtained from the source plugin configuration.
   *
   * @var array[]
   *   Array of objects, articles selected for migration.
   */
  protected $articles = [];

  /**
   * Drupal\watwat\WatWatApiService.
   *
   * @var \Drupal\watwat\WatWatApiService
   */
  protected $watwatApiService;

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var |Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * WatwatParagraphQuote constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory,
                              Connection $connection,
                              EntityTypeManagerInterface $entityTypeManager) {
    $this->watwatApiService = $watwat_api_service;
    $this->loggerFactory = $logger_factory;
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;

    // Get the ids that are marked for migration.
    $article_ids = $this->connection->select('watwat_addtomigration', 'n')
      ->fields('n', ['watwat_source_id'])
      ->execute()
      ->fetchAll();

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    // Get the articles that are linked to the ids.
    foreach ($article_ids as $id) {
      $ids[] = $id->watwat_source_id;
    }
    $articles = [];
    if (!empty($ids)) {
      // Only 10 articles can be retrieved per call.
      $articles['total'] = count($ids);
      $array_chunks = array_chunk($ids, 10);
      foreach ($array_chunks as $chunk) {
        $article_string = implode(',', $chunk);
        $article_array = $this->watwatApiService->getArticles(['id' => $article_string]);
        foreach ($article_array->results as $article) {
          $articles['results'][] = $article;
        }
      }
    }
    $this->articles = $articles;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('watwat.apiService'),
      $container->get('logger.factory'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Initializes the iterator with the source data.
   *
   * @return \Iterator
   *   Returns an iteratable object of data for this source.
   */
  public function initializeIterator() {
    return new \ArrayIterator($this->dataRows);
  }

  /**
   * Allows class to decide how it will react when it is treated like a string.
   */
  public function __toString() {
    return '';
  }

  /**
   * Gets the source count.
   *
   * Return a count of available source records, from the cache if appropriate.
   * Returns -1 if the source is not countable.
   *
   * @param bool $refresh
   *   (optional) Whether or not to refresh the count. Defaults to FALSE. Not
   *   all implementations support the reset flag. In such instances this
   *   parameter is ignored and the result of calling the method will always be
   *   up to date.
   *
   * @return int
   *   The count.
   */
  public function count($refresh = FALSE) {
    return count($this->dataRows);
  }

  /**
   * Returns available fields on the source.
   *
   * @return array
   *   Available fields in the source, keys are the field machine names as used
   *   in field mappings, values are descriptions.
   */
  public function fields() {
    return [
      'Nid' => 'Node id',
      'Original id' => 'Original id',
      'Weight' => 'Weight',
    ];
  }

  /**
   * Defines the source fields uniquely identifying a source row.
   *
   * @return array[]
   *   An associative array of field definitions keyed by field ID.
   */
  public function getIds() {
    return [
      'Nid' => [
        'type' => 'integer',
      ],
      'Original id' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * Transform a string to remove images, special characters etc.
   *
   * @param string $original_string
   *   The original string.
   *
   * @return string
   *   The transformed string.
   */
  public function transformString($original_string) {
    // Replace WAT WAT inline links with the full url.
    $watwat_base_url = $GLOBALS['WATWAT_BASE_URL'];
    $url_prefix = 'href="' . $watwat_base_url;
    $updated_string = str_replace('href="/', $url_prefix, $original_string);

    // Remove images.
    $updated_string_no_images = preg_replace('/<img[^>]+\>/i', '', $updated_string);

    // Decode special characters.
    $transformed_string = htmlspecialchars_decode($updated_string_no_images);

    return $transformed_string;
  }

}
