<?php

namespace Drupal\watwat_paragraphs\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\watwat\WatWatApiService;
use Drupal\watwat\WatwatMissingSourceInterface;
use Drupal\watwat\WatwatMissingSourceTrait;

/**
 * Source plugin for retrieving the attachment paragraphs.
 *
 * @MigrateSource(
 *   id = "watwat_paragraph_attachment"
 * )
 */
class WatwatParagraphAttachment extends WatwatParagraphSourceBase implements ContainerFactoryPluginInterface, WatwatMissingSourceInterface {

  use WatwatMissingSourceTrait;

  /**
   * WatwatParagraphAttachment constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory,
                              Connection $connection,
                              EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $watwat_api_service, $logger_factory, $connection, $entityTypeManager);

    $paragraphs = [];
    if (!empty($this->articles)) {
      foreach ($this->articles['results'] as $article) {
        $article_content = $article->content;
        foreach ($article_content as $paragraph) {
          if ($paragraph->type == 'attachment') {
            $paragraphs[] = [
              'Nid' => $article->id,
              'Original id' => $paragraph->id,
              'Weight' => $paragraph->weight,
              'Attachments' => $paragraph->content->attachments,
            ];
          }
        }
      }
    }
    $this->dataRows = $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'Nid' => 'Node id',
      'Original id' => 'Original id',
      'Weight' => 'Weight',
      'Attachments' => 'Attachments',
    ];
  }

  /**
   * Adds additional data to the row.
   *
   * @param \Drupal\Migrate\Row $row
   *   The row object.
   *
   * @return bool
   *   FALSE if this row needs to be skipped.
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    $target_ids = [];
    $database_table = 'migrate_map_watwat_paragraph_attachment_item_migration';

    // Get the target id and the target revision id for each attachment.
    foreach ($row->getSourceProperty('Attachments') as $attachment) {
      if ($this->connection->schema()->tableExists($database_table) === TRUE) {
        $results = $this->connection->select($database_table, 'yt')
          ->fields('yt', ['destid1', 'destid2'])
          ->condition('yt.sourceid1', $row->getSourceProperty('Nid'), '=')
          ->condition('yt.sourceid2', $attachment->title, '=')
          ->execute()
          ->fetchAll();
        if (!empty($results)) {
          foreach ($results as $result) {
            $target_ids[] = [
              'target_id' => $result->destid1,
              'target_revision_id' => $result->destid2,
            ];
          }
        }
      }
      else {
        // Log an error.
        $message = 'The database table ' . $database_table . ' was not found.';
        $this->loggerFactory->get('watwat')->error($message);
      }
    }

    // Add the target ids as a source property for the row.
    $row->setSourceProperty('prepare_attachment', $target_ids);
    return parent::prepareRow($row);
  }

}
