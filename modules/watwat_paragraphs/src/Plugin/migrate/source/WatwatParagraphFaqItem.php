<?php

namespace Drupal\watwat_paragraphs\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\watwat\WatWatApiService;
use Drupal\watwat\WatwatMissingSourceInterface;
use Drupal\watwat\WatwatMissingSourceTrait;

/**
 * Source plugin for retrieving the faq items paragraphs.
 *
 * @MigrateSource(
 *   id = "watwat_paragraph_faq_item"
 * )
 */
class WatwatParagraphFaqItem extends WatwatParagraphSourceBase implements ContainerFactoryPluginInterface, WatwatMissingSourceInterface {

  use WatwatMissingSourceTrait;

  /**
   * WatwatParagraphFaqItem constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory,
                              Connection $connection,
                              EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $watwat_api_service, $logger_factory, $connection, $entityTypeManager);

    $paragraphs = [];
    if (!empty($this->articles)) {
      foreach ($this->articles['results'] as $article) {
        $article_content = $article->content;
        foreach ($article_content as $paragraph) {
          if ($paragraph->type == 'faq') {
            $faq_items = $paragraph->content->items;
            foreach ($faq_items as $faq_item) {
              $question = strip_tags($faq_item->question);
              $question_stripped = str_replace('&nbsp;', ' ', $question);
              $question_fully_stripped = str_replace(["\r\n", "\n\n"], '', $question_stripped);
              $final_question = htmlspecialchars_decode($question_fully_stripped);
              $paragraphs[] = [
                'Nid' => $article->id,
                'Original id' => $faq_item->id,
                'Question' => $final_question,
                'Answer' => $faq_item->answer,
              ];
            }

          }
        }
      }
    }
    $this->dataRows = $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'Nid' => 'Node id',
      'Original id' => 'Original id',
      'Question' => 'Question',
      'Answer' => 'Answer',
    ];
  }

  /**
   * Defines the source fields uniquely identifying a source row.
   *
   * @return array[]
   *   An associative array of field definitions keyed by field ID.
   */
  public function getIds() {
    return [
      'Nid' => [
        'type' => 'integer',
      ],
      'Original id' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * Update the internal links to external links.
   *
   * @param \Drupal\Migrate\Row $row
   *   The row object.
   *
   * @return bool
   *   FALSE if this row needs to be skipped.
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    $source_answer = $row->getSourceProperty('Answer');
    if ($source_answer) {
      $transformed_answer = $this->transformString($source_answer);
      $row->setSourceProperty('Text', $transformed_answer);
    }

    return parent::prepareRow($row);
  }

}
