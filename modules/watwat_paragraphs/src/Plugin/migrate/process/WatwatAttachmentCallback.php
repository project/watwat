<?php

namespace Drupal\watwat_paragraphs\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\Callback;
use Drupal\migrate\Row;

/**
 * Processes the source value to remove the url.
 *
 * @MigrateProcessPlugin(
 *   id = "watwat_attachment_callback"
 * )
 */
class WatwatAttachmentCallback extends Callback {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value != NULL) {
      $value = substr($value, strrpos($value, '/') + 1);
    }
    return $value;
  }

}
