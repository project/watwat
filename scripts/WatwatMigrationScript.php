<?php

/**
 * @file
 * Contains the Wat Wat Migration Script.
 */

use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;

$migrate_list = [
  'watwat_paragraph_text_migration',
  'watwat_paragraph_video_migration',
  'watwat_paragraph_quote_migration',
  'watwat_paragraph_html_embed_migration',
  'watwat_paragraph_faq_item_migration',
  'watwat_paragraph_faq_migration',
  'watwat_paragraph_image_file_migration',
  'watwat_paragraph_image_media_migration',
  'watwat_paragraph_image_migration',
  'watwat_paragraph_cta_file_migration',
  'watwat_paragraph_cta_media_migration',
  'watwat_paragraph_call_to_action_migration',
  'watwat_paragraph_attachment_file_migration',
  'watwat_paragraph_attachment_item_migration',
  'watwat_paragraph_attachment_migration',
  'watwat_article_image_file_migration',
  'watwat_article_image_media_migration',
  'watwat_article_migration',
];

$manager = \Drupal::service('plugin.manager.migration');
foreach ($migrate_list as $migrate_id) {
  $migration = $manager->createInstance($migrate_id);
  $executable = new MigrateExecutable($migration, new MigrateMessage());
  $executable->import();

  // Get data to display a message.
  $map = $migration->getIdMap();
  $errors = $map->errorCount();
  $processed = $map->processedCount();
  $status = $migration->getStatusLabel();

  $message = 'The ' . $migrate_id . ' processed a total of ' . $processed . ' item(s) and has returned ' . $errors . ' error(s). The status is now ' . $status . '.';
  \Drupal::logger('watwat_migration')->notice($message);
}
