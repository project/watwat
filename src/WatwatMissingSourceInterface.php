<?php

namespace Drupal\watwat;

use Drupal\migrate\Plugin\MigrateSourceInterface;

/**
 * Interface WatwatMissingSourceInterface.
 *
 * @package Drupal\watwat
 */
interface WatwatMissingSourceInterface extends MigrateSourceInterface {

  /**
   * Rewinds the iterator for unpublishing missing source values.
   */
  public function rewindForUnpublish();

  /**
   * Determines next row.
   *
   * This function will skip "track_changes" and "highWaterMark" checks.
   * We need the full source to determine which rows to unpublish.
   */
  public function nextNoValidation();

}
