<?php

namespace Drupal\watwat\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;
use Drupal\watwat\WatWatApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WatwatUrlBase.
 *
 * @package Drupal\watwat\Plugin\migrate\source
 */
class WatwatUrlBase extends Url implements ContainerFactoryPluginInterface {

  /**
   * Drupal\watwat\WatWatApiService.
   *
   * @var \Drupal\watwat\WatWatApiService
   */
  protected $watwatApiService;

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * WatwatUrlBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\watwat\WatWatApiService $watwat_api_service
   *   The Wat Wat api service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              MigrationInterface $migration,
                              WatWatApiService $watwat_api_service,
                              LoggerChannelFactory $logger_factory,
                              Connection $connection) {

    $this->watwatApiService = $watwat_api_service;
    $this->loggerFactory = $logger_factory;
    $this->connection = $connection;

    // Get the ids that are marked for migration.
    $article_ids = $this->connection->select('watwat_addtomigration', 'n')
      ->fields('n', ['watwat_source_id'])
      ->execute()
      ->fetchAll();

    // Set the base url for the separate article migrations.
    if (!empty($configuration['urls'])) {
      $base_url = $configuration['urls'];
      $configuration['urls'] = [];
    }

    // Add a separate url for every article id.
    if (!empty($base_url)) {
      foreach ($article_ids as $article) {
        $configuration['urls'][] = $base_url . '?id=' . $article->watwat_source_id;
      }
    }
    else {
      // Log an error.
      $message = 'No url was configured for the Wat Wat article migration.';
      $this->loggerFactory->get('watwat')->error($message);
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * Creates an instance of the class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container interface.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface|null $migration
   *   The migration interface.
   *
   * @return WatwatUrlBase|static
   *   The Wat Wat url base.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('watwat.apiService'),
      $container->get('logger.factory'),
      $container->get('database')
    );
  }

}
