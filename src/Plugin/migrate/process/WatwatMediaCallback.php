<?php

namespace Drupal\watwat\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\Callback;
use Drupal\migrate\Row;

/**
 * Processes the source value to remove the query parameters.
 *
 * @MigrateProcessPlugin(
 *   id = "watwat_media_callback"
 * )
 */
class WatwatMediaCallback extends Callback {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value != NULL) {
      $parsed_url = parse_url($value);
      $path = explode('/', $parsed_url['path']);
      $value = end($path);
    }
    return $value;
  }

}
