<?php

namespace Drupal\watwat\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\Callback;
use Drupal\migrate\Row;

/**
 * Processes the source value to remove the query parameters.
 *
 * @MigrateProcessPlugin(
 *   id = "watwat_file_callback"
 * )
 */
class WatwatFileCallback extends Callback {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value != NULL) {
      $value = substr($value, 0, strpos($value, '?'));
    }
    return $value;
  }

}
