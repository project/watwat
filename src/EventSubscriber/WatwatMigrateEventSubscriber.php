<?php

namespace Drupal\watwat\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\watwat\WatwatMissingSourceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class WatwatMigrateEventSubscriber.
 *
 * @package Drupal\watwat\EventSubscriber
 */
class WatwatMigrateEventSubscriber implements EventSubscriberInterface {

  const DERIVATIVE_SEPARATOR = ':';

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MigrateEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[MigrateEvents::PRE_IMPORT][] = ['onMigratePreImport'];
    return $events;
  }

  /**
   * Callback function for "MigrateEvents::PRE_IMPORT" event.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onMigratePreImport(MigrateImportEvent $event) {
    $migration = $event->getMigration();

    $source = $migration->getSourcePlugin();
    if (!$source instanceof WatwatMissingSourceInterface) {
      return;
    }

    $id_map = $migration->getIdMap();
    $id_map->prepareUpdate();

    $source->rewindForUnpublish();
    $source_id_values = [];

    while ($source->valid()) {
      $source_id_values[] = $source->current()->getSourceIdValues();
      $source->nextNoValidation();
    }
    $id_map->rewind();

    $destination = $migration->getDestinationPlugin();
    $plugin_id = $destination->getPluginId();
    $entity_type_id = '';
    if (strpos($plugin_id, static::DERIVATIVE_SEPARATOR)) {
      list(, $entity_type_id) = explode(static::DERIVATIVE_SEPARATOR, $plugin_id, 2);
    }
    if (!$entity_type_id) {
      return;
    }

    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!$entity_type->hasKey('published')) {
      return;
    }

    if (!$storage = $this->entityTypeManager->getStorage($entity_type_id)) {
      return;
    }

    while ($id_map->valid()) {
      $map_source_id = $id_map->currentSource();
      if (in_array($map_source_id, $source_id_values)) {
        $id_map->next();
        continue;
      }

      $destination_ids = $id_map->currentDestination();
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if (!$entity = $storage->load(reset($destination_ids))) {
        $id_map->next();
        continue;
      }

      // Remove the item if it is not found in the source.
      $entity->delete();

      $id_map->next();
    }
  }

}
