<?php

namespace Drupal\watwat;

use Drupal\migrate\Row;

/**
 * Trait WatwatMissingSourceTrait.
 *
 * @package Drupal\vibmigrate
 */
trait WatwatMissingSourceTrait {

  /**
   * {@inheritdoc}
   */
  public function rewindForUnpublish() {
    $this->getIterator()->rewind();
    $this->nextNoValidation();
  }

  /**
   * {@inheritdoc}
   */
  public function nextNoValidation() {
    $this->currentSourceIds = NULL;
    $this->currentRow = NULL;

    // In order to find the next row we want to process, we ask the source
    // plugin for the next possible row.
    while (!isset($this->currentRow) && $this->getIterator()->valid()) {
      $row_data = $this->getIterator()->current() + $this->configuration;
      $this->fetchNextRow();
      $row = new Row($row_data, $this->getIds());

      // Populate the source key for this row.
      $this->currentSourceIds = $row->getSourceIdValues();

      // Pick up the existing map row, if any, unless fetchNextRow() did it.
      if (!$this->mapRowAdded && ($id_map = $this->idMap->getRowBySource($this->currentSourceIds))) {
        $row->setIdMap($id_map);
      }

      $this->prepareRow($row);
      $this->currentRow = $row->freezeSource();
    }
  }

}
