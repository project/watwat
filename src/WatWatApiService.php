<?php

namespace Drupal\watwat;

use GuzzleHttp\Client;

/**
 * Service that handles all the API calls for the WatWat module.
 */
class WatWatApiService {

  /**
   * The Wat wat api url.
   *
   * @var string
   *   The api url.
   */
  protected $apiUrl = 'https://www.watwat.be/api/v1/';

  /**
   * The GuzzleHttp\Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * WatWatApiService constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client service.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Get all WatWat themes.
   *
   * @return array
   *   The array of themes.
   */
  public function getThemes() {
    $themes = [];
    $params['offset'] = 0;
    $params['amount'] = 100;

    // The amount of themes per api call is limited to 100.
    // Keep making the api call until you have all the themes.
    do {
      $api_themes = $this->getResults('themes', $params);

      // Transform the received themes into a list with the id as key.
      if (!empty($api_themes)) {
        foreach ($api_themes->results as $theme) {
          $themes[$theme->id] = $theme->title;
        }
      }
      $params['offset'] += 100;
    } while (count($api_themes->results) > 99);

    return $themes;
  }

  /**
   * Get the title for a specific theme.
   *
   * @param int $theme_id
   *   The theme id.
   *
   * @return array
   *   The theme data.
   */
  public function getTheme($theme_id) {
    $params['id'] = $theme_id;
    $theme_data = $this->getResults('themes', $params);
    return reset($theme_data->results) ? reset($theme_data->results) : [];
  }

  /**
   * Get the WatWat articles.
   *
   * @param array $params
   *   The array of parameters for the api call.
   *
   * @return mixed
   *   Returns the requested articles.
   */
  public function getArticles(array $params) {
    $articles = $this->getResults('articles', $params);

    return $articles;
  }

  /**
   * Function that calls the api and get the requested results.
   *
   * @param string $entity
   *   The entity for which the api call is made.
   * @param array $params
   *   The array of parameters for the api call.
   *
   * @return mixed
   *   The requested results.
   */
  public function getResults($entity, array $params) {
    if (!empty($entity)) {
      $suffix = '?';

      // Add the parameters for the call.
      if (!empty($params)) {
        foreach ($params as $key => $param) {
          if ($key == 'themes') {
            $param = str_replace('+', ',', $param);
          }
          $suffix .= $key . '=' . $param . '&';
        }
      }

      // Get the results of the composer api call url.
      $client = $this->httpClient;
      $request = $client->get($this->apiUrl . $entity . $suffix);
      $response = $request->getBody();

      return json_decode($response);
    }
  }

}
